CREATE TABLE cameras 
(
    id varchar(100) NOT NULL,
	lat numeric,
	lon numeric,
	PRIMARY KEY (id)
);

CREATE TABLE alerts
(
    id varchar(100) NOT NULL,
    alert_time timestamp NOT NULL,
    frequency numeric NOT NULL,
    spectrum_pic text NOT NULL,
	camera_id varchar(100) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (camera_id) REFERENCES cameras(id)
);

CREATE TABLE vods 
(
    id varchar(50) NOT NULL,
    start_date timestamp NOT NULL,
	end_date timestamp NOT NULL,
	camera_id varchar(100) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (camera_id) REFERENCES cameras(id)
);

CREATE TABLE entities 
(
    id varchar(100) NOT NULL,
	entity_type int NOT NULL,
	description varchar(10) NOT NULL,
	vod_id varchar(50) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (vod_id) REFERENCES vods(id)
);

CREATE SEQUENCE TS_timestamp
MINVALUE 0
MAXVALUE 999999999999999999999999999
INCREMENT BY 10;