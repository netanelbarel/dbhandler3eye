package DBHandler.services;

import DBHandler.repositories.CameraRepository;
import DBHandler.models.Camera;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CameraService
{
    @Autowired
    private CameraRepository cameraRepository;

    public List<Camera> getAll()
    {
        return this.cameraRepository.findAll();
    }
    public Camera add(Camera camera)
    {
        return this.cameraRepository.save(camera);
    }
    public Camera getById(String id) {
        return this.cameraRepository.getOne(id);
    }
    public Camera update(Camera camera) {
        return this.cameraRepository.save(camera);
    }
}
