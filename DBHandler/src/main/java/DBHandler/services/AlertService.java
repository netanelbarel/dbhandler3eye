package DBHandler.services;

import DBHandler.models.Vod;
import DBHandler.repositories.AlertRepository;
import DBHandler.models.Alert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class AlertService
{
    @Autowired
    private AlertRepository alertRepository;

    public Collection<Alert> getAll()
    {
        return this.alertRepository.findAll();
    }

    public Collection<Alert> getAllAlertsInTimeRange(Date startDate, Date endDate, String cameraId) {
        return this.alertRepository.getAllAlertsInTimeRange(startDate, endDate, cameraId);
    }

    public Collection<Alert> getAllAlertsFromTime(Date startDate, String cameraId) {
        return this.alertRepository.getAllAlertsFromTime(startDate, cameraId);
    }

    public void addAlert(Alert alert){
        this.alertRepository.save(alert);
    }
}
