package DBHandler.services;

import DBHandler.models.Vod;
import DBHandler.repositories.VodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class VodService
{

    @Autowired
    private VodRepository vodRepository;

    public List<Vod> getAll()
    {
        return this.vodRepository.findAll();
    }

    public void addVodMetaData(Vod metaData){
        this.vodRepository.save(metaData);
    }

    public Collection<Vod> getAllVodMetaDataInTimeRange(Date startDate, Date endDate, String cameraId){
        Collection<Vod> vodMetaDataList = this.vodRepository.getAllVodMetaDataInTimeRange(startDate, endDate, cameraId);
        return vodMetaDataList;
    }
}
