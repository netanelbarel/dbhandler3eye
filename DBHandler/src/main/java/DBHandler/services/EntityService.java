package DBHandler.services;

import DBHandler.models.Entity;
import DBHandler.repositories.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class EntityService
{
    @Autowired
    private EntityRepository entityRepository;

    public List<Entity> getAll()
    {
        return this.entityRepository.findAll();
    }
}
