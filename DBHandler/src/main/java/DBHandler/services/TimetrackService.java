package DBHandler.services;

import DBHandler.models.Timetrack;
import DBHandler.repositories.TimetrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class TimetrackService {

    @Autowired
    private TimetrackRepository timetrackRepository;

    public List<Timetrack> getAll() {
        return this.timetrackRepository.findAll();
    }

    public Long getTimeTrack(String cameraId) throws Exception {
        Collection<Long> cameraTimestemp = timetrackRepository.getCameraTimestemp(cameraId);
        if (cameraTimestemp.size() != 1) throw new Exception("Size was not 1");
        return (Long) cameraTimestemp.toArray()[0];
    }

    public Collection<Long> raiseTimetrack(String cameraId) {
        timetrackRepository.raiseTimetrack(cameraId);
        return null;
    }
}
