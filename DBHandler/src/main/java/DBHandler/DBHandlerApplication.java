package DBHandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

class TaskDto
{
	private int id;
	private String text;
	private Boolean done;

	public TaskDto(int i, String t, boolean b)
	{
		id = i;
		text = t;
		done = b;
	}

	public int getId()
	{
		return id;
	}
	public String getText()
	{
		return text;
	}
	public Boolean getDone()
	{
		return done;
	}
}


@SpringBootApplication
@RestController
public class DBHandlerApplication
{
	public static void main(String [] args)
	{
		SpringApplication.run(DBHandlerApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name)
	{
		return String.format("Hello %s!", name);
	}

	@GetMapping("/task")
	public String task()
	{
		return "[]";
	}

	@PostMapping("/post")
	public TaskDto postTask(@RequestBody TaskDto params)
	{
		return new TaskDto(params.getId(), params.getText(), params.getDone());
	}

	@GetMapping("/post/{postId}")
	public TaskDto getTask(@PathVariable(name = "postId") int id)
	{
		return new TaskDto(id, "lidor", false);
	}
}