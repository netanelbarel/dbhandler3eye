package DBHandler.controllers;

import DBHandler.models.Entity;
import DBHandler.services.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping(value = "/entity")
public class EntityController
{
    @Autowired
    private EntityService entityService;

    @GetMapping
    public Collection<Entity> getAllEntities()
    {
        return this.entityService.getAll();
    }
}