package DBHandler.controllers.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class VodDto{
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
    public Date startDate;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
    public Date endDate;
    public String cameraId;
}
