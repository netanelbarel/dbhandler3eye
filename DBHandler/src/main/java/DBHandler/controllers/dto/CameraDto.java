package DBHandler.controllers.dto;

public class CameraDto {
    private String id;
    private Double lon;
    private Double lat;

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public String getId() {
        return id;
    }
}
