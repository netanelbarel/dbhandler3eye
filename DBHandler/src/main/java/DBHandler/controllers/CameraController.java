package DBHandler.controllers;

import DBHandler.controllers.dto.CameraDto;
import DBHandler.models.Camera;
import DBHandler.services.CameraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "camera")
public class CameraController
{
    @Autowired
    private CameraService cameraService;

    @GetMapping("")
    public List<Camera> getAllCameras()
    {
        return this.cameraService.getAll();
    }

    @PostMapping("")
    public Camera addCamera(@RequestBody CameraDto cameraDto)
    {
        Camera camera = new Camera(cameraDto.getLat(), cameraDto.getLon());
        return this.cameraService.add(camera);
    }

    @GetMapping("{id}")
    public Camera geCameraById(@PathVariable(name="id") String id)
    {
        return this.cameraService.getById(id);
    }

    @PutMapping("")
    public Camera updateCamera(@RequestBody CameraDto cameraDto)
    {
        if(cameraDto.getId() == null) {
            // TODO throw error (better than this, with status code etc..)
            throw new RuntimeException("Missing id");
        }
        Camera camera = this.cameraService.getById(cameraDto.getId());
        camera.setLon(cameraDto.getLon());
        camera.setLat(cameraDto.getLat());

        return this.cameraService.update(camera);
    }
}