package DBHandler.controllers;

import DBHandler.models.Timetrack;
import DBHandler.services.TimetrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = "/timetrack")
public class TimetrackController {
    @Autowired
    private TimetrackService timetrackService;

    @GetMapping
    public Collection<Timetrack> getAllTracks() {
        return this.timetrackService.getAll();
    }

    @PostMapping("raiseTrack")
    public Long getTimeTrack(@RequestBody String cameraId) throws Exception {
        Long current = timetrackService.getTimeTrack(cameraId);
        timetrackService.raiseTimetrack(cameraId);
        return current;
    }
}