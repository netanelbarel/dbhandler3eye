package DBHandler.controllers;

import DBHandler.controllers.dto.VodDto;
import DBHandler.models.Vod;
import DBHandler.services.VodService;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;

@RestController
@RequestMapping(value = "/vod")
public class VodController
{
    @Autowired
    private VodService vodService;

    @GetMapping
    public Collection<Vod> getAllVodMetaData()
    {
        return this.vodService.getAll();
    }

    @PostMapping("create")
    public void addVodMetaData(@RequestBody Vod metaData)
    {
        this.vodService.addVodMetaData(metaData);
    }

    @PostMapping("getVodInTimeRange")
    public Collection<Vod> postFilter(@RequestBody VodDto vodParams)
    {
        return this.vodService.getAllVodMetaDataInTimeRange(vodParams.startDate, vodParams.endDate, vodParams.cameraId);
    }
}
