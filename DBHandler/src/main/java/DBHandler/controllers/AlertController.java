package DBHandler.controllers;

import DBHandler.models.Alert;
import DBHandler.models.Vod;
import DBHandler.services.AlertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;

@RestController
@RequestMapping(value = "/alert")
public class AlertController
{
    @Autowired
    private AlertService alertService;

    @GetMapping
    public Collection<Alert> getAllAlerts()
        {
            return this.alertService.getAll();
        }

    @PostMapping("getAlertsInTimeRange")
    public Collection<Alert> postFilter(@RequestBody AlertsParams alertsParams)
    {
        return this.alertService.getAllAlertsInTimeRange(alertsParams.startDate, alertsParams.endDate, alertsParams.cameraId);
    }

    @PostMapping("getAllAlertsFromTime")
    public Collection<Alert> getAllAlertsFromTime(@RequestBody AlertsStartTimeParams alertsStartTimeParams) {
        return this.alertService.getAllAlertsFromTime(alertsStartTimeParams.startDate, alertsStartTimeParams.cameraId);
    }

    @PostMapping("create")
    public void addVodMetaData(@RequestBody Alert alert)
    {
        this.alertService.addAlert(alert);
    }

    class AlertsParams{
        public Date startDate;
        public Date endDate;
        public String cameraId;
    }

    class AlertsStartTimeParams{
        public Date startDate;
        public String cameraId;
    }
}

