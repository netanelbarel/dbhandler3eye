package DBHandler.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "vods")
public class Vod
{
    @Id
    @Column(name = "id")
    @JsonProperty
    private String id;

    @Column(name = "start_date")
    @JsonProperty
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
    private Date startDate;

    @Column(name = "end_date")
    @JsonProperty
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
    private Date endDate;

    @Column(name = "camera_id")
    @JsonProperty
    private String cameraId;
}
