package DBHandler.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "alerts")
public class Alert
{
    @Id
    @Column(name = "id")
    @JsonProperty
    private String id;

    @Column(name = "alert_time")
    @JsonProperty
    private Date alertTime;

    @Column(name = "frequency")
    @JsonProperty
    private long frequency;

    @Column(name = "camera_id")
    @JsonProperty
    private String cameraId;

    @Column(name = "spectrum_pic")
    @JsonProperty
    private String spectrumPic;
}
