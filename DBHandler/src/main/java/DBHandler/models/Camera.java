package DBHandler.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "cameras")
public class Camera
{
    @Id
    @Column(name = "id")
    @JsonProperty
    private String id;

    @Column(name = "lat")
    @JsonProperty
    private double lat;

    @Column(name = "lon")
    @JsonProperty
    private double lon;

    public Camera(double lat, double lon) {
        String uuid = UUID.randomUUID().toString();
        this.id = uuid;
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
