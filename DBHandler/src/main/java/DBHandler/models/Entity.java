package DBHandler.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "entities")
public class Entity
{
    @Id
    @Column(name = "id")
    @JsonProperty
    private String entityId;

    @Column(name = "vod_id")
    @JsonProperty
    private String vodId;

    @Column(name = "entity_type")
    @JsonProperty
    private int entityType;

    @Column(name = "description")
    @JsonProperty
    private String description;
}
