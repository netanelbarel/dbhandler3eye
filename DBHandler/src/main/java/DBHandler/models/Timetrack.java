package DBHandler.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "timetrack")
public class Timetrack
{
    @Id
    @Column(name = "camera_id")
    @JsonProperty
    private String cameraId;

    @SequenceGenerator(name = "ts_timestemp", sequenceName = "ts_timestemp")
    @Column(name = "ts_timestemp")
    @JsonProperty
    private Long timestemp;

}
