package DBHandler.repositories;

import DBHandler.models.Vod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Repository
public interface VodRepository extends JpaRepository<Vod, Long> {
    @Query("SELECT cameraId, startDate, endDate, id FROM Vod WHERE cameraId = ?3 AND ((startDate <= ?1 AND endDate >= ?1) OR (startDate <= ?2 AND endDate >= ?2) OR (startDate >= ?1 AND endDate <= ?2))")
    Collection<Vod> getAllVodMetaDataInTimeRange(Date startDate, Date endDate, String cameraId);
}
