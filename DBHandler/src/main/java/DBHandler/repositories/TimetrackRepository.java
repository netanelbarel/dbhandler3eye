package DBHandler.repositories;

import DBHandler.models.Timetrack;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Repository
public interface TimetrackRepository extends JpaRepository<Timetrack, Long> {
    @Query(value = "SELECT ts_timestemp FROM timetrack WHERE camera_id = ?1", nativeQuery = true)
    Collection<Long> getCameraTimestemp(String cameraId);

    @Transactional
    @Modifying
    @Query(value = "UPDATE timetrack SET ts_timestemp = ts_timestemp + 10 WHERE camera_id = ?1", nativeQuery = true)
    int raiseTimetrack(String cameraId);

}