package DBHandler.repositories;

import DBHandler.models.Alert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Date;

public interface AlertRepository extends JpaRepository <Alert, String> {
    @Query("SELECT cameraId, alertTime, id, frequency, spectrumPic FROM Alert WHERE cameraId = ?3 AND alertTime >= ?1 AND alertTime <= ?2")
    Collection<Alert> getAllAlertsInTimeRange(Date startDate, Date endDate, String cameraId);

    @Query("SELECT cameraId, alertTime, id, frequency, spectrumPic FROM Alert WHERE cameraId = ?2 AND alertTime >= ?1")
    Collection<Alert> getAllAlertsFromTime(Date startDate, String cameraId);
}
