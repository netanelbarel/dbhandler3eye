package DBHandler.repositories;

import DBHandler.models.Camera;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface CameraRepository extends JpaRepository<Camera, String> {}