package DBHandler.repositories;

import DBHandler.models.Entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface EntityRepository extends JpaRepository<Entity, String> {}
